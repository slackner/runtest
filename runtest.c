#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <pty.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define EXIT_TIMEOUT    124
#define EXIT_SIGNAL     125
#define EXIT_CRITICAL   126

static int process_fd;
static int process_pid;
static int process_running;
static int process_status;

/* set process exit status */
void set_status(int status)
{
    int flags = fcntl(process_fd, F_GETFL, 0);
    fcntl(process_fd, F_SETFL, flags | O_NONBLOCK);
    process_status  = status;
    process_running = 0;
}

/* kill process, then set status to timeout */
void handle_sigalrm(int sig)
{
    int saved_errno = errno;
    if (process_running)
    {
        kill(process_pid, SIGKILL);
        set_status(EXIT_TIMEOUT);
    }
    errno = saved_errno;
}

/* determine exit code of child process */
void handle_sigchld(int sig)
{
    int saved_errno = errno;
    int ret, status;
    do
    {
        ret = waitpid((pid_t)-1, &status, WNOHANG);
        if (ret == process_pid && process_running)
        {
            if (WIFEXITED(status))
                set_status(WEXITSTATUS(status));
            else if (WIFSIGNALED(status))
                set_status(EXIT_SIGNAL);
            else
                set_status(EXIT_CRITICAL);
        }
    }
    while (ret >= 0);
    errno = saved_errno;
}

int main(int argc, char *argv[])
{
    sigset_t mask, old_mask;
    struct sigaction sa;
    pid_t pid;
    int fd;

    assert(argv[argc] == NULL);
    if (argc < 2)
    {
        printf("Usage: %s command [arguments ...]\n\n", argv[0]);
        return 1;
    }

    /* block SIGCHLD until we have forked */
    if (sigemptyset(&mask) ||
        sigaddset(&mask, SIGCHLD) ||
        sigprocmask(SIG_BLOCK, &mask, &old_mask))
    {
        perror("blocking SIGCHLD failed\n");
        return 1;
    }

    /* create tty */
    pid = forkpty(&fd, NULL, NULL, NULL);
    if (pid == -1)
    {
        perror("forkpty failed");
        return 1;
    }

    /* child process */
    if (pid == 0)
    {
        close(fd);
        sigprocmask(SIG_SETMASK, &old_mask, NULL);
        execvp(argv[1], &argv[1]);
        perror("execvp failed");
        return 1;
    }

    /* parent process */
    process_fd      = fd;
    process_pid     = pid;
    process_running = 1;
    process_status  = EXIT_CRITICAL;

    /* install a signal handler for SIGALRM */
    sa.sa_handler = &handle_sigalrm;
    sa.sa_flags = 0;
    if (sigemptyset(&sa.sa_mask) ||
        sigaction(SIGALRM, &sa, 0))
    {
        perror("setting SIGALRM handler failed");
        kill(pid, SIGKILL);
        close(fd);
        return 1;
    }

    /* install a signal handler for SIGCHLD */
    sa.sa_handler = &handle_sigchld;
    sa.sa_flags = SA_NOCLDSTOP;
    if (sigemptyset(&sa.sa_mask) ||
        sigaction(SIGCHLD, &sa, 0) ||
        sigprocmask(SIG_SETMASK, &old_mask, NULL))
    {
        perror("setting SIGCHLD handler failed");
        kill(pid, SIGKILL);
        close(fd);
        return 1;
    }

    /* set alarm */
    alarm(300);

    for (;;)
    {
        char buffer[1024];
        int len, ret;

        /* read a new chunk of data */
        do
        {
            len = read(fd, &buffer, sizeof(buffer));
        }
        while (len < 0 && errno == EINTR);

        if (len < 0)
        {
            if (process_running)
            {
                /* It could be a race-condition, maybe the pipe was closed
                 * before the child process terminated. Give another 0.5 sec
                 * in this case, to avoid printing false errors */
                if (errno == EIO)
                {
                    int i;
                    for (i = 0; process_running && i < 100; i++)
                        usleep(5000);
                    if (!process_running) break;
                    errno = EIO;
                }
            }
            else
            {
                /* EAGAIN is normal when we the pipe is empty, but the child
                 * process is still running. EIO occurs when the child process
                 * has terminated. */
                if (errno == EAGAIN || errno == EIO)
                    break;
            }
            perror("read failed");
            break;
        }

        while (len)
        {
            do
            {
                ret = write(STDOUT_FILENO, buffer, len);
            }
            while (ret == -1 && errno == EINTR);

            if (ret < 0)
            {
                perror("write failed");
                goto out;
            }

            if (ret < len) memmove(buffer, &buffer[ret], len - ret);
            len -= ret;
        }
    }

out:
    /* block SIGCHLD/SIGALRM again */
    sigaddset(&mask, SIGALRM);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    /* if the process is still running kill it */
    if (process_running)
    {
        set_status(EXIT_CRITICAL);
        kill(pid, SIGKILL);
    }

    close(fd);
    return process_status;
}